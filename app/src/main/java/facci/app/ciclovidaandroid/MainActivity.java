package facci.app.ciclovidaandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //nos permite enviar msj a una app en android studio
        Log.e("Ciclo de Vida", "onCreate");
        //Crearemos un atributo
        MostrarMensaje("onCreate");
    }
    private void MostrarMensaje(String mensaje){
        Toast.makeText( this, mensaje, Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onStart(){
        super.onStart();
        Log.e("Ciclo de vida", "onStart");
        MostrarMensaje("onStart");
    }


    @Override
    protected void onResume(){
        super.onResume();
        Log.e("Ciclo de vida", "onResume");
        MostrarMensaje("onResume");
    }


    @Override
    protected void onPause(){
        super.onPause();
        Log.e("Ciclo de vida", "onPause");
        MostrarMensaje("onPause");
    }


    @Override
    protected void onStop(){
        super.onStop();
        Log.e("Ciclo de vida", "onStop");
        MostrarMensaje("onStop");
    }


    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.e("Ciclo de vida", "onDestroy");
        MostrarMensaje("onDestroy");
    }


    @Override
    protected void onRestart(){
        super.onRestart();
        Log.e("Ciclo de vida", "onRestart");
        MostrarMensaje("onRestart");
    }
}




